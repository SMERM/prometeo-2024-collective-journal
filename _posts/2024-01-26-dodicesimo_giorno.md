---
layout: post
author: Nicola Bernardini
title:  "Dodicesimo giorno (26/01/2024)"
date:   2024-01-26 21:00:14 +0200
categories: recita
---

In mattinata presentazione della mostra *Prometeo possibili* realizzata dagli
studenti dell'Accademia di Belle Arti in collaborazione con l'Archivio Luigi
Nono. Bella mostra, splendida documentazione della progettazione del primo
*Prometeo*.

| ![Mostra 1](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240126-nicb/1706344460019.jpg) |
|:--:|
| *Fig.1 - Mostra *Prometeo possibili* * |

| ![Mostra 2](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240126-nicb/1706344460012.jpg) |
|:--:|
| *Fig.2 - Mostra *Prometeo possibili* * |

| ![Mostra 3](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240126-nicb/1706344460007.jpg) |
|:--:|
| *Fig.3 - Mostra *Prometeo possibili* - da sinistra: Deborah Rossi, Serena Nono, Riccardo Caldura, Giorgio Mastinu* |

Come diceva Haller, oggi *Prometeo* vola da solo. Tutto il lavoro che c'era da
fare, che era possibile fare, è stato fatto. Da un lavoro del genere c'è
sempre molto da imparare, e ogni esecuzione - in prova o in concerto che sia -
presenta problematiche diverse da risolvere creando sul momento, ossia
"interpretando" il momento per quello che è e per quello che vale.

La sorpresa di oggi è che il pubblico "asciuga" molto la chiesa - più di
quello che era ragionevole aspettarsi. Il suono cambia, ed è difficile tenere
la barra dritta. Ma alla fine si arriva in fondo con un'esecuzione
praticamente perfetta sul piano tecnico, e anche molto apprezzata dal pubblico
sul piano musicale. Si parla di una "riscoperta" del *Prometeo*, della sua
"rinascita" e amenità varie. Si protesta un po' per il poco rispetto dei
silenzi e dei pianissimissimi da parte della crew tecnica delle riprese video,
ma alla fine si va a casa contenti. *Prometeo* versione 2024, nella chiesa per
la quale è stato creato, è nato.

| ![Postazione Live-Electronics 1](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240126-nicb/1706344460002.jpg) |
|:--:|
| *Fig.4 - *Prometeo* Postazione Live-Electronics 1 (Vidolin) * |

| ![Postazione Live-Electronics 2](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240126-nicb/1706344459997.jpg) |
|:--:|
| *Fig.5 - *Prometeo* Postazione Live-Electronics 2 (Richelli) * |

| ![Postazione Live-Electronics 3](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240126-nicb/1706344459990.jpg) |
|:--:|
| *Fig.6 - *Prometeo* Postazione Live-Electronics 3 (Bernardini) * |

| ![Vidolin Bernardini](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240126-nicb/1706344459984.jpg) |
|:--:|
| *Fig.7 - Alvise Vidolin e Nicola Bernardini, poco prima dell'inizio della prima * |
