---
layout: post
author: Nicola Bernardini
title:  "Decimo giorno (24/01/2024)"
date:   2024-01-24 21:00:14 +0200
categories: allestimento
---

La mattinata è iniziata con la partecipazione a un convegno dedicato al
*Prometeo* alla Fondazione Cini all'isola di San Giorgio. La sessione 
prevedeva una tavola rotonda con la partecipazione di Veniero Rizzardi nella
funzione di moderatore, Marco Angius, Alvise Vidolin e Giancarlo Schiaffini.

| ![Tavola Rotonda alla Fondazione Giorgio Cini](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240124-nicb/1706133598394.jpg) |
|:--:|
| *Fig.1 - Tavola Rotonda alla Fondazione Cini - da sinistra a destra: Giancarlo Schiaffini, Alvise Vidolin, Marco Angius, Veniero Rizzardi* |

La giornata è poi continuata con una prova "filata" del *Prometeo*, della
durata di 2 ore e 20 minuti, seguita da alcune messe a punto di passaggi
specifici - prima con le orchestre e poi con il solo coro e i solisti.
