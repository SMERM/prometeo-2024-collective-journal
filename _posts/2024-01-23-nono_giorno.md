---
layout: post
author: Nicola Bernardini
title:  "Nono giorno (23/01/2024)"
date:   2024-01-23 21:00:14 +0200
categories: allestimento
---

Prima prova con tutti: 4 orchestre, coro, cast vocale, voci recitanti,
solisti, campane di vetro. Ora le tessere del puzzle si stanno combinando e il
risultato è sempre più impressionante. In serata si provano le luci e si
cominciano a provare le entrate e le uscite dalla struttura - cosa abbastanza
compicata tenendo conto del numero di elementi coinvolti.

Oggi sono arrivati gli studenti un po' da tutta Italia. Hanno seguito tutta la
prova attenendosi alle rigorose indicazioni e condizioni della produzione per
poter assistere. In realtà la chiesa comincia a riempirsi anche di alti
papaveri e persone importanti in genere. La prima si sta avvicinando.

Lascio ai nostri studenti proseguire questo diario - riservandomi di
aggiungere elementi salienti ove opportuno.
