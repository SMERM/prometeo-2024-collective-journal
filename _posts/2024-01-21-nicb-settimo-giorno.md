---
layout: post
author: Nicola Bernardini
title:  "Settimo giorno (21/01/2024)"
date:   2024-01-21 21:00:14 +0200
categories: allestimento
---

Anche oggi giornata intensa di prove. Marco Angius ha provato praticamente
tutte le parti di soli, cast, coro e voci recitanti, saltando le parti per
orchestra che verranno affrontate nelle prove di domani.

Oggi finalmente si è capita la relazione musicale tra il *Prometeo* e la
chiesa di San Lorenzo. Il *Prometeo* è stato concepito *nello* spazio di San
Lorenzo, e in questo spazio vengono in luce tutte le innumerevoli sfumature
della scrittura. Emozionante.

La messa a punto è ancora in fieri, ma ogni giorno vengono fatti passi avanti.
