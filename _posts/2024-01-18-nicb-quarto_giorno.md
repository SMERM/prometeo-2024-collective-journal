---
layout: post
author: Nicola Bernardini
title:  "Quarto giorno (18/01/2024)"
date:   2024-01-18 21:00:14 +0200
categories: allestimento
---

Il quarto giorno è stato più movimentato e faticoso. Nelle prime due ore della
mattinata abbiamo condotto una prova tecnica utilizzando le tracce di Parma in
*virtual soundcheck*, riscontrando ancora qualche piccola imprecisione qua e
là.

Poi siamo dovuti correre a Ca' Giustinian (sede della Biennale) per
partecipare alla conferenza stampa di presentazione del progetto. La presenza
più importante è stata quella di Nuria Schönberg-Nono (vedova del compositore
e figlia di Arnold Schönberg) presente assieme alla figlia Serena. Nuria ha
fatto un bel discorso menzionando la fortuna di Nono nell'essersi circondato
sempre di persone di alto profilo. Sono poi intervenuti Marco Angius che ha
ripercorso la forma del Prometeo e infine Alvise che ha difeso questo nuovo
allestimento in linea con l'estetica noniana.

| ![Sala delle colonne](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705585841575.jpg) |
|:--:|
| *Fig.1 - Conferenza stampa in Ca' Giustinian, sala delle colonne* |

| ![Nuria Schönberg](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705585841569.jpg) |
|:--:|
| *Fig.2 - Nuria Schőnberg-Nono* |

| ![AV e Serena Nono](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705585841564.jpg) |
|:--:|
| *Fig.3 - Alvise Vidolin, Serena Nono* |

| ![Marco Angius](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705585841556.jpg) |
|:--:|
| *Fig.4 - Marco Angius* |

| ![Alvise Vidolin](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705585841551.jpg) |
|:--:|
| *Fig.5 - Alvise Vidolin* |


Immediatamente dopo la conferenza stampa c'è stato un sopralluogo approfondito
in cui si è deciso di spostare i tre fiati solisti (flauto, clarinetto e
tuba/trombone) dalla postazione decisa inizialmente - troppo angusta - a
quella che era stata assegnata alle 2 voci recitanti.

| ![Angius, Braga, Vidolin](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705585841546.jpg) |
|:--:|
| *Fig.6 - Marco Angius, Michele Braga, Alvise Vidolin* |

| ![Vista dalla postazione dei soli](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705585841526.jpg) |
|:--:|
| *Fig.7 - Vista dalla postazione dei fiati solisti* |

| ![Vista dalla postazione dei soli](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705585841533.jpg) |
|:--:|
| *Fig.8 - Vista dalla postazione dei fiati solisti* |

| ![Vista dalla postazione dei soli](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705585841540.jpg) |
|:--:|
| *Fig.9 - Vista dalla postazione dei fiati solisti* |

Dopo il sopralluogo abbiamo fatto una filata tecnica "all'italiana" (ossia
verificando solamente tutti i cambi scena) prima di scappare per un impegno
serale a Padova. Dulcis in fundo è comparso Giancarlo Schiaffini.

| ![Schiaffini, Viola, Vidolin](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240118-nicb/1705618282084.jpg) |
|:--:|
| *Fig.10 - Giancarlo Schiaffini, Antonino Viola, Alvise Vidolin* |
