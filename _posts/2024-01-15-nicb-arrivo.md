---
layout: post
author: Nicola Bernardini
title:  "Primo giorno (15/01/2024)"
date:   2024-01-15 20:32:14 +0200
categories: allestimento
---

Siamo arrivati verso le 11 a S.Lorenzo.
I lavori di costruzione della struttura sono piuttosto indietro - anche se
sembrano avanzare rapidamente. Si tratta di una struttura di tubi innocenti,
e io temo assai per il rumore (nei camminamenti e nel suonare). Staremo a
vedere. Abbiamo definito la posizione degli altoparlanti.

E siamo tornati a casa (di Alvise) a lavorare. Il sistema è completamente
nuovo, basato su una singola macchina Mac M1 ridondata via protocollo *Dante*
con un'altra macchina. Una terza macchina serve per inviare (MIDI-over-IP) i
controlli a entrambe le macchine. Il problema è spegnere e riaccendere: ogni
volta si perde una qualche parte della configurazione e bisogna riconfigurare
tutto daccapo. Io non so perché tolleriamo questa sciatteria dei programmatori di software
per la musica - ma non c'è verso di convincere i miei compagni che ciò sia
intollerabile. Va bene. Io nel frattempo ho ri-segnato un'intera partitura
(ritirata vergine stamane da Stecchini a Padova). E risegnato quasi tutto. Mi
mancano le evidenziazioni, le farò domani.

| ![Panorama](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240115-nicb/1705353864696.jpg) |
|:--:|
| *Fig.1 - Panorama pic dalla postazione di regia - Emisfero A* |

| ![d&b](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240115-nicb/1705354230997.jpg) |
|:--:|
| *Fig.2 - Gli array D&B a terra* |

| ![d&b](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240115-nicb/1705354231004.jpg) |
|:--:|
| *Fig.3 - Gli array D&B a terra* |

| ![Piano](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240115-nicb/1705354231010.jpg) |
|:--:|
| *Fig.4 - Piano del posizionamento dei diffusori (Alvise Vidolin e Massimo Carli - BH)* |

| ![Persone](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240115-nicb/1705354231016.jpg) |
|:--:|
| *Fig.5 - Da sinistra a destra: Michele Braga, Massimo Carli, Alvise Vidolin* |

| ![Emisfero A](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240115-nicb/1705354231022.jpg) |
|:--:|
| *Fig.6 - Emisfero A* |

| ![Emisfero A](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240115-nicb/1705354231027.jpg) |
|:--:|
| *Fig.7 - Emisfero A visto dall'ingresso (passata la struttura)* |

| ![Emisfero A](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240115-nicb/1705354231034.jpg)|
|:--:|
| *Fig.8 - Emisfero A visto dall'ingresso (prima della struttura)* |
