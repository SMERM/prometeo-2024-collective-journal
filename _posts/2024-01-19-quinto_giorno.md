---
layout: post
author: Nicola Bernardini
title:  "Quinto giorno (19/01/2024)"
date:   2024-01-19 21:00:14 +0200
categories: allestimento
---

Nella mattinata abbiamo installato le campane di vetro e abbiamo fatto le
prove di rito: il suono reale di queste campane non si dovrebbe udire, solo le
loro trasformazioni (shift di due ottave verso il grave e riverbero). E'
un'operazione complicata che richiede molto lavoro.

| ![Vetri 1](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240119-nicb/1705664344697.jpg) |
|:--:|
| *Fig.1 - Campane Varotto, versione 2024* |

| ![Vetri 2](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240119-nicb/1705664344685.jpg) |
|:--:|
| *Fig.2 - Campane Varotto, versione 2024, con Luca Richelli e Alvise Vidolin* |

| ![Vetri 3](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240119-nicb/1705664344674.jpg) |
|:--:|
| *Fig.3 - Campane Varotto, versione 2024* |

| ![Vetri 4](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240119-nicb/1705664344660.jpg) |
|:--:|
| *Fig.4 - Posizione delle campane viste dal basso, con Christian, Alvise e il percussionista Saverio Ruffo* |

| ![Vetri 5](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240119-nicb/1705706265735.jpg) |
|:--:|
| *Fig.5 - Microfonamento a contatto delle campane (microfoni a contatto Schertler)* |

| ![Vetri 6](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240119-nicb/1705706265739.jpg) |
|:--:|
| *Fig.6 - Microfonamento a contatto delle campane (microfoni a contatto Schertler)* |

Alle cinque iniziano le prime prove con gli strumentisti. Oggi solisti (archi e fiat) e coro.
Si tratta più che altro di prove tecniche (regolazione dei guadagni dei
microfoni, prove di passaggi specifici. Vengono provati soprattutto i *gate*
cioè quei passaggi in cui gli strumentisti, soffiando nel proprio strumento,
aprono e chiudono le amplificazioni di altri strumenti o altre voci.

| ![Sistemazione fiati solisti](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240119-nicb/1705706265731.jpg) |
|:--:|
| *Fig.7 - Sistemazione dei fiati solisti (visibile sulla destra: Alvise Vidolin)* |

| ![Fiati solisti](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240119-nicb/1705706265726.jpg) |
|:--:|
| *Fig.8 - I fiati solisti - dal fondo: Giancarlo Schiaffini, Roberta Gottardi, Roberto Fabbriciani* |

| ![Coro](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240119-nicb/1705706265721.jpg) |
|:--:|
| *Fig.9 - Sistemazione del coro* |
