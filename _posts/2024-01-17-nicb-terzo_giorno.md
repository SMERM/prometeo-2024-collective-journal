---
layout: post
author: Nicola Bernardini
title:  "Terzo giorno (17/01/2024)"
date:   2024-01-17 20:07:14 +0200
categories: allestimento
---

San Lorenzo ci accoglie con un gradito tepore, anche se è più l'impressione
dopo la camminata per arrivare sino a qui.

L'audio è ormai installato e pronto. Cominciamo a sentire i primi suoni in
mezzo alle martellate e alle urla degli operai che lavorano intorno a noi.
Pranzo dai pakistani poco lontano con l'intera crew BH, molto bravi e
competenti.
Nel pomeriggio cominciamo a sistemare tutte le sequenze seguendo la partitura.
Abbiamo le tracce separate degli strumenti dall'edizione del 2017 a Parma e
con quelle cominciamo a sentire i primi risultati del live-electronics
versione 2024. I tavoli della regìa, che sembravano così grandi, cominciano a
riempirsi di roba. Oltre ai nostri computer, ai controller, al mixer
e alle due partiture che dobbiamo utilizzare, tutte le superfici si popolano
di intercom col direttore, monitor aggiuntivo per seguire le scene, monitor
per seguire il secondo direttore, ecc. Stiamo diventando un albero di natale
fuori tempo. Alla fine della giornata siamo arrivati al primo *Stasimo*.
Domattina dovremmo completare tutti i movimenti e nel pomeriggio dovremmo fare
una prima filata tecnica. Chissà. Ma è tutto tranquillo.

| ![Postazione Live-Electronics 3 - 3° giorno](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240117-nicb/1705519176366.jpg) |
|:--:|
| *Fig.1 - Live-Electronics: postazione 3* |

