---
layout: post
author: Nicola Bernardini
title:  "Ottavo giorno (22/01/2024)"
date:   2024-01-22 21:00:14 +0200
categories: allestimento
---

Oggi prove con le 4 orchestre, cast vocale, soli strumentali e attori. La
struttura si è caricata di altri 52 elementi ma sembra reggere bene. Le
orchestre fanno risaltare la trasparenza di questo allestimento. La
distribuzione delle orchestre nei due emisferi è particolarmente efficace. La prova è
stata intensa, ripercorrendo tutti i passaggi di pieno strumentale che sono
abbastanza impressionanti, nonostante i sincroni siano difficili da ottenere e
abbiano ancora bisogno di altro lavoro.

Alla sera si comincia ad affrontare il discorso delle luci che si rivela
subito complicato: è complicato combinare le esigenze dei musicisti (che hanno
bisogno di vedere le parti senza essere accecati dai riflettori) con quelle
scenografiche - sembra difficile accordare esigenze funzionali ed estetiche.
Staremo a vedere.

| ![Orchestra 1](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240122-nicb/1705999991385.jpg) |
|:--:|
| *Fig.1 - Emisfero A, Orchestra 1* |

| ![Cast vocale](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240122-nicb/1705999991374.jpg) |
|:--:|
| *Fig.2 - Cast Vocale* |

| ![Orchestra 4](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240122-nicb/1705999991379.jpg) |
|:--:|
| *Fig.3 - Emisfero A, Orchestra 4* |
