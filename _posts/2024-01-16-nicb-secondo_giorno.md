---
layout: post
author: Nicola Bernardini
title:  "Secondo giorno (16/01/2024)"
date:   2024-01-16 16:25:14 +0200
categories: allestimento
---

Oggi abbiamo trasportato le nostre apparecchiature a San Lorenzo e abbiamo
montato la regìa. Abbiamo ancora qualche problema di sincronizzazione di tutti
i controller ma sostanzialmente funziona tutto. Nonostante i pannelli
riscaldanti e i termoconvettori il freddo dentro San Lorenzo si fa sentire.
Nel frattempo si continua a lavorare alacremente all'allestimento della struttura.

| <video width="640" height="480" controls> <source src="https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240116-nicb/VID_20240116_102613.webm" type="video/webm"> <source src="https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240116-nicb/VID_20240116_102613.ogv" type="video/ogg"> <source src="https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240116-nicb/VID_20240116_102613.mp4" type="video/mp4"> </video> |
|:--:|
| *Video 1 - Video dell'Emisfero A realizzato per il Direttore Marco Angius* |

| <video width="640" height="480" controls> <source src="https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240116-nicb/VID_20240116_103658.webm" type="video/webm"> <source src="https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240116-nicb/VID_20240116_103658.ogv" type="video/ogg"> <source src="https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240116-nicb/VID_20240116_103658.mp4" type="video/mp4"> </video> |
|:--:|
| *Video 2 - Video dell'Emisfero B realizzato per il Direttore Marco Angius* |

| ![Regìa 1](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240116-nicb/1705425718159.jpg) |
|:--:|
| *Fig.1 - La regìa al secondo giorno (di spalle da sinistra: Christian, Alvise che nasconde Luca)* |

| ![Regìa 2](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240116-nicb/1705425718171.jpg) |
|:--:|
| *Fig.2 - La regìa al secondo giorno (di profilo: Christian, Alvise che nasconde Luca)* |

| ![Regìa 3](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240116-nicb/1705425718470.jpg) |
|:--:|
| *Fig.3 - La regìa al secondo giorno* |
