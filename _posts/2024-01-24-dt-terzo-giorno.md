---
layout: post
author: Davide Tedesco
title:  "Terzo giorno (24/01/2024)"
date:   2024-01-24 21:00:00 +0200
categories: prove, allestimento
---

Il seguente diario è stato scritto su carta, fotografato e riportato per intero per tutti i giorni della mia permanenza a Venezia. Esso è realizzato in forma di lettere il cui destinatario è Luigi Nono. Il diario è stato realizzato quasi sempre il giorno seguente a quello menzionato.

| ![Pagina 1](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240124-dt/20240124_pagina1.jpg) |
|:--:|
| *Fig.1 - Pagina 1 del diario del 24 gennaio 2024* |

| ![Pagina 2](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240124-dt/20240124_pagina2.jpg) |
|:--:|
| *Fig.2 - Pagina 2 del diario del 24 gennaio 2024* |
