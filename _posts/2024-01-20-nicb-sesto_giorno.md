---
layout: post
author: Nicola Bernardini
title:  "Sesto giorno (20/01/2024)"
date:   2024-01-20 21:00:14 +0200
categories: allestimento
---

Giornata intensa di prove. In mattinata, prove tecniche per stabilire i
livelli del cast di cantanti solisti (2 soprano, 2 contralto e un tenore).
Nel pomeriggio prove musicali con gli attori

| ![Attori](https://gitlab.com/SMERM/prometeo-2024-collective-journal/-/raw/master/images/20240120-nicb/1705774290704.jpg) |
|:--:|
| *Fig.1 - Gli attori* |

e poi con tutti gli strumenti solisti (flauto, clarinetto, trombone/tuba,
viola, violoncello, contrabbasso) diretti dal secondo direttore M°Filippo Perocco.
