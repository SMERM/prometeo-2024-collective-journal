---
layout: page
title: Che cos'è
permalink: /about/
---

Questo è il diario collettivo del gruppo SMERM (Scuola di Musica Elettronica di Roma)
scritto in occasione della preparazione e della produzione del *Prometeo* di Luigi Nono
nella chiesa di San Lorenzo a Venezia nel gennaio del 2024.
